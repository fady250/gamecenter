package server.database;

import misc.Player;
import misc.ScoreEntries;

public interface IDatabaseManager {

    void initializeDB();
    boolean checkIfUserExist(String username);
    void registerUser(String username , String password);
    String queryPassword(String username);
    int addActiveGame(String game, Player p1, Player p2);
    void removeActiveGame(int id);
    void updateTurn(int id);
    Player getNextPlayer(int id);
    Player getCurrentPlayer(int id);
    String getGameType(int id);
    void updateScoreTables(int id, String username);
    ScoreEntries getScoreTable(String game);
}
