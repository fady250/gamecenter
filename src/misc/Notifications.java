package misc;

public enum Notifications {
    YOUR_TURN,
    OPPONENT_QUIT,      // opponent closed game window during game
    YOU_LOST,           // sent for the losing player - the winner knows that ...
    GAME_TIE
}
