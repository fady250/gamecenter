package client.model;

import misc.Status;

public interface IUserModel {
    Status registerUser(String username, String password);
    Status loginUser(String username, String password);
    void logoutUser(String username);
}



