package client;

import client.core.GameCenterApp;
import javafx.application.Application;

public class RunGameCenterClient {
    public static void main(String[] args) {
        Application.launch(GameCenterApp.class);
    }
}