package client.core;

import client.model.GameModelImpl;
import client.model.IGameModel;
import client.model.IUserModel;
import client.model.UserModelImpl;


public class ModelFactory {
    private final ClientFactory cf;
    private IUserModel userModel;
    private IGameModel gameModel;

    public ModelFactory(ClientFactory cf) {
        this.cf = cf;
    }

    // return it by lazy instantiation ( only if someone asks it --> create and return it )
    // also always return privates (so we return the same model)
    public IUserModel getUserModel() {
       if(userModel ==  null){
           userModel = new UserModelImpl(cf.getClient());
       }
       return userModel;
    }

    public IGameModel getGameModel() {
        if(gameModel ==  null){
            gameModel = new GameModelImpl(cf.getClient());
        }
        return gameModel;
    }
}
