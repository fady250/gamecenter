package client.networking;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import misc.*;


import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/*
 * IClient - i/f that is exposed to the model which is a layer on top
 * IRMIClient - i/f that is exposed to the server which gives the server the ability to run methods on the client (callback)
 */
public class RMIClient implements IClient, IRMIClient {

    private IRMIServer server;              // stub representing the server
    private int id;
    private final StringProperty userName = new SimpleStringProperty();
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public RMIClient()  {
        try {
            UnicastRemoteObject.exportObject(this, 0);      // expose client to RMI
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        this.id = -1;
    }

    @Override
    public void addListener(String eventName, PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(eventName,listener);
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    @Override
    public void startClient() {
        try {
            Registry registry = LocateRegistry.getRegistry("localhost",1099);   // ip , port#
            server = (IRMIServer) registry.lookup("GameCenterServer");              // connect by name
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Status registerUser(String username, String password) {
        try {
            return server.registerUser(username,password);
        } catch (RemoteException e) {
            throw new RuntimeException("Could not contact server");
        }
    }

    @Override
    public Status loginUser(String username, String password) {
        try {
            return server.loginUser(username,password,this);
        } catch (RemoteException e) {
            throw new RuntimeException("Could not contact server");
        }
    }

    @Override
    public void logoutUser(String username) {
        try {
            server.logoutUser(username);
        } catch (RemoteException e) {
            throw new RuntimeException("Could not contact server");
        }
    }

    @Override
    public void insertPlayerIntoGameQueue(String game) {
        try {
            server.insertPlayerIntoGameQueue(game, this);
        }catch (RemoteException e){
            throw new RuntimeException("Could not contact server");
        }
    }

    @Override
    public void updateGameState(GameState state) {
        try {
            server.updateGameState(id,state);
        }catch (RemoteException e){
            throw new RuntimeException("Could not contact server");
        }
    }

    @Override
    public void gameWindowClosed(String game) {
        try {
            server.gameWindowClosed(game,id);
        }catch (RemoteException e){
            throw new RuntimeException("Could not contact server");
        }
    }

   @Override
    public ScoreEntries getScores(String game) {
       try {
           return server.getScores(game);
       } catch (RemoteException e) {
           e.printStackTrace();
       }
       return null;
    }


    // below are implementations of the methods in the IRMIClient interface - these can be called by the server (callback)

    @Override
    public void updateID(int id) throws RemoteException {
        this.id = id;
        System.out.println("id updated" + id);
    }

    @Override
    public void setRole(char role) throws RemoteException {
        changeSupport.firePropertyChange("RoleUpdate",null,role);
    }

    @Override
    public void notify(Notifications type) throws RemoteException {
        if(type.equals(Notifications.YOUR_TURN)){
            changeSupport.firePropertyChange("GrantTurn",null,null);
        }else {
            Platform.runLater(() -> changeSupport.firePropertyChange("UpdateUserMsg",null,type));
        }
    }

    @Override
    public void updateGui(GameState state) throws RemoteException {
        Platform.runLater(() -> changeSupport.firePropertyChange("GuiUpdate",null,state));
    }

    @Override
    public String getUsername() throws RemoteException {
        return userName.get();
    }
}
