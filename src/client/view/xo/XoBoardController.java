package client.view.xo;

import client.core.ViewManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;


public class XoBoardController {

    public Label userMessage;
    //private ViewManager vManager;
    private XoBoardViewModel boardVM;

    @FXML Button b1;
    @FXML Button b2;
    @FXML Button b3;
    @FXML Button b4;
    @FXML Button b5;
    @FXML Button b6;
    @FXML Button b7;
    @FXML Button b8;
    @FXML Button b9;
    @FXML GridPane gameBoard;

    public void init(ViewManager viewManager, XoBoardViewModel boardVM ){
        //this.vManager = viewManager;
        this.boardVM = boardVM;
        b1.textProperty().bindBidirectional(boardVM.b1Property());
        b2.textProperty().bindBidirectional(boardVM.b2Property());
        b3.textProperty().bindBidirectional(boardVM.b3Property());
        b4.textProperty().bindBidirectional(boardVM.b4Property());
        b5.textProperty().bindBidirectional(boardVM.b5Property());
        b6.textProperty().bindBidirectional(boardVM.b6Property());
        b7.textProperty().bindBidirectional(boardVM.b7Property());
        b8.textProperty().bindBidirectional(boardVM.b8Property());
        b9.textProperty().bindBidirectional(boardVM.b9Property());
        gameBoard.disableProperty().bindBidirectional(boardVM.gridDisableProperty());
        userMessage.textProperty().bindBidirectional(boardVM.userMessageProperty());
        b1.setText(" ");
        b2.setText(" ");
        b3.setText(" ");
        b4.setText(" ");
        b5.setText(" ");
        b6.setText(" ");
        b7.setText(" ");
        b9.setText(" ");
        b8.setText(" ");
    }

    public void windowClosed(){
        boardVM.windowClosed();
    }

    public void buttonClickHandler(ActionEvent evt){
        boardVM.handleClick((Button) evt.getTarget());
    }

    //todo fire event to here
    private void highlightWinningCombo(Button first, Button second, Button third){
        first.getStyleClass().add("winning-button");
        second.getStyleClass().add("winning-button");
        third.getStyleClass().add("winning-button");
    }
}
