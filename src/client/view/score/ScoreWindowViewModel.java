package client.view.score;

import client.model.IGameModel;
import javafx.collections.ObservableList;
import misc.ScoreEntry;

public class ScoreWindowViewModel {

    private final IGameModel gameModel;


    public ScoreWindowViewModel(IGameModel gameModel) {
        this.gameModel = gameModel;
    }

    public ObservableList<ScoreEntry> getScores(String game) {
        return gameModel.getScores(game);
    }
}
