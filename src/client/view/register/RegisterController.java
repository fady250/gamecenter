package client.view.register;

import client.core.ViewManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class RegisterController {

    @FXML
    public TextField username;
    @FXML
    public TextField password;
    @FXML
    public TextField confirmPassword;
    @FXML
    public Button cancelButton;
    @FXML
    public Button registerButton;
    @FXML
    public Label registerResponse;

    private ViewManager vm;
    private RegisterViewModel registerVM;


    public void init(RegisterViewModel registerVM, ViewManager viewManager) {
        this.vm = viewManager;
        this.registerVM = registerVM;
        username.textProperty().bindBidirectional(registerVM.usernameTextProperty());
        password.textProperty().bindBidirectional(registerVM.passwordTextProperty());
        confirmPassword.textProperty().bindBidirectional(registerVM.confirmPasswordTextProperty());
        registerResponse.textProperty().bind(registerVM.registerResponseTextProperty());             // bind - whenever property changes in the VM its reflected to the GUI (controller)
        cancelButton.textProperty().bind(registerVM.cancelButtonTextProperty());
        registerButton.disableProperty().bind(registerVM.registerButtonDisableProperty());
    }

    public void RegisterClicked(ActionEvent actionEvent) {
        registerVM.registerUser();
    }


    public void CancelClicked(ActionEvent actionEvent) {
        //vm.updateRegisterWindowOpenedVar();
        registerVM.clear();
        // get a handle to the stage
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }
}
